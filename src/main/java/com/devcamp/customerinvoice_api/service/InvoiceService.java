package com.devcamp.customerinvoice_api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customerinvoice_api.model.Invoice;
@Service
public class InvoiceService {
    CustomerService customerService = new CustomerService();
    
    Invoice invoice1 = new Invoice(001, customerService.getCustomer1(), 15000000);
    Invoice invoice2 = new Invoice(002, customerService.getCustomer2(), 18000000);
    Invoice invoice3 = new Invoice(003, customerService.getCustomer3(), 20000000);

    public ArrayList<Invoice> getAllInvoice(){
        ArrayList<Invoice> listInvoice = new ArrayList<>();
        listInvoice.add(invoice1);
        listInvoice.add(invoice2);
        listInvoice.add(invoice3);
        return listInvoice;
    }
}
