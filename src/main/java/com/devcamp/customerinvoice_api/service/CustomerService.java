package com.devcamp.customerinvoice_api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customerinvoice_api.model.Customer;
@Service
public class CustomerService {
    Customer customer1 = new Customer(01, "Nguyen Van A", 10);
    Customer customer2 = new Customer(02, "Nguyen Van B", 12);
    Customer customer3 = new Customer(03, "Nguyen Van C", 15);

    public ArrayList<Customer> getAllCustomer(){
        ArrayList<Customer> listCustomer = new ArrayList<>();
        listCustomer.add(customer1);
        listCustomer.add(customer2);
        listCustomer.add(customer3);
        return listCustomer;
    }

    public Customer getCustomer1() {
        return customer1;
    }

    public Customer getCustomer2() {
        return customer2;
    }

    public Customer getCustomer3() {
        return customer3;
    }
}
