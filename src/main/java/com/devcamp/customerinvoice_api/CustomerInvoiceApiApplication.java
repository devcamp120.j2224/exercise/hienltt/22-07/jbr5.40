package com.devcamp.customerinvoice_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerInvoiceApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerInvoiceApiApplication.class, args);
	}

}
