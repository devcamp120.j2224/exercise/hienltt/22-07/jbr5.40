package com.devcamp.customerinvoice_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerinvoice_api.model.Invoice;
import com.devcamp.customerinvoice_api.service.InvoiceService;

@RestController
public class InvoiceController {
    @Autowired
    InvoiceService invoiceService;

    @CrossOrigin
    @GetMapping("/invoices")
    public ArrayList<Invoice> getListInvoice(){
        ArrayList<Invoice> lInvoices = invoiceService.getAllInvoice();
        return lInvoices;
    }
}
