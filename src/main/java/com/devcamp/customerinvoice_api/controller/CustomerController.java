package com.devcamp.customerinvoice_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerinvoice_api.model.Customer;
import com.devcamp.customerinvoice_api.service.CustomerService;

@RestController
public class CustomerController {
    @Autowired
    CustomerService customerService;
    
    @CrossOrigin
    @GetMapping("/customers")
    public ArrayList<Customer> getListCustomer(){
        ArrayList<Customer> lCustomer = customerService.getAllCustomer();
        return lCustomer;
    }
}
